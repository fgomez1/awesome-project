import React from 'react';
import { Chart, LineAdvance } from 'bizcharts';

import { PieChart } from 'bizcharts';

// 数据源
const data = [
  {
    type: 'Allowed',
    value: 100,
  },
  {
    type: 'Abandoned',
    value: 27,
    colorField: 'gray',
  },
];

function Demo() {
  return (
    <div style={{ width: 450 }}>
      <PieChart
        data={data}
        description={{
          visible: true,
          text:
            'Examiner Allowance vs Class Allowance',
        }}
        radius={0.8}
        angleField="value"
        colorField="type"
        label={{
          visible: true,
          type: 'outer',
          offset: 20,
        }}
      />
    </div>
  );
}

export default Demo;
